#include "opencl_api.h"
#include "logger.h"

using namespace std;

cl_int GetPlatformList(cl_platform_id* platformIds, cl_uint numOfPlatforms, vector<string> &platformList)
{
  cl_int ret = 0;

  // Temporary buffer to store device information
  vector<char> deviceNameBuf(MAX_PLATFORM_NAME);

  for (int i = 0; i < numOfPlatforms; i++)
  {
    if (ret = clGetPlatformInfo(platformIds[i], CL_PLATFORM_NAME, MAX_PLATFORM_NAME, deviceNameBuf.data(), NULL))
    {
      LOG_ERROR("Can not get platform info");
      continue;
    }

    // Add info to list
    platformList.push_back(deviceNameBuf.data());
  }

  return ret;
}

cl_int CompileKernelCode(const char* sourcePath, cl_context context, cl_device_id device_id, cl_kernel &kernel)
{
  cl_int ret = 0;

  FILE* kernelSrc = NULL;

  if (auto err = fopen_s(&kernelSrc, sourcePath, "r"))
  {
    LOG_ERROR("Can not open file with path: " << sourcePath);
    return err;
  }

  char * source_str = (char*)malloc(256);
  size_t source_size = fread(source_str, 1, 256, kernelSrc);

  // Close source file
  fclose(kernelSrc);

  // Build binary file from program
  cl_program program = clCreateProgramWithSource(context, 1, (const char**)&source_str, (const size_t*)&source_size, &ret);

  // Free allocated memory
  free(source_str);

  // Compile program
  ret = clBuildProgram(program, 1, &device_id, NULL, NULL, NULL);

  // Create kernel
  kernel = clCreateKernel(program, "test", &ret);

  // Specify kernel args
  cl_int arg = 0;

  clSetKernelArg(kernel, 0, sizeof(cl_int), &arg);

  if (ret != 0)
  {
    return ret;
  }

  return ERROR_SUCCESS;
}

cl_int StartKernelCode(cl_command_queue queue, cl_kernel kernel, cl_event &clEvent)
{
  cl_int ret = 0;
  size_t callCount = 1;


  if (ret = clEnqueueTask(queue, kernel, 0, NULL, &clEvent))
  {
    return ret;
  }

  return ERROR_SUCCESS;
}