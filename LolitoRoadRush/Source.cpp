#include "logger.h"
#include "opencl_api.h"

using namespace std;

int test_function(char* kernelCodePath)
{
  cl_int ret = 0;
  cl_int uret = 0;
  cl_uint numOfDevices = 0;
  cl_uint numOfPlatforms = 0;

  // Id structs
  cl_device_id   deviceId = {};
  cl_platform_id platformIds[MAX_PLATFORM_SIZE] = {};

  // Device specific 
  cl_context context = {};
  cl_command_queue cmdQueue = {};

  cl_kernel kernel = {};
  cl_event  clEvent = {};

  // Get availiable platforms
  if (ret = clGetPlatformIDs(MAX_PLATFORM_SIZE, platformIds, &numOfPlatforms))
  {
    return ret;
  }

  if (numOfPlatforms == 0)
  {
    LOG_WARNING("No supported platforms was found. Check your device for OpenCl compatibility!");
  }
  else
  {
    LOG_INFO(numOfPlatforms << " availiable platforms was found");
  }

  LOG_INFO("Print list of availiable platforms");

  int chosenPlatform = 0;

  cout << "-------------------------------------------------------------------------------" << endl;

  vector<string> platformList = {};
  GetPlatformList(platformIds, numOfPlatforms, platformList);

  for (auto platformName : platformList)
    cout << "\t" << platformName << endl;

  cout << "Choose your destiny: ";
  cin >> chosenPlatform;

  cout << "-------------------------------------------------------------------------------" << endl;

  // Get availiable devices
  if (ret = clGetDeviceIDs(platformIds[chosenPlatform], CL_DEVICE_TYPE_ALL, 1, &deviceId, &numOfDevices))
  {
    LOG_ERROR("Can not get device for provided platform! Check device parameters.");
    return ret;
  }

  // Create device context
  context = clCreateContext(NULL, 1, &deviceId, NULL, NULL, &ret);

  if (ret)
  {
    LOG_ERROR("Failed to create device context!");
    return ret;
  }

  // Get command queue
  cmdQueue = clCreateCommandQueueWithProperties(context, deviceId, 0, &ret);

  // Compile kernel code
  LOG_INFO("Compile kernel code start...");

  if (ret = CompileKernelCode(kernelCodePath, context, deviceId, kernel) != ERROR_SUCCESS)
  {
    LOG_ERROR("Failed to compile kernel code!");
    return ret;
  }

  LOG_INFO("Succesfuly compiled kernel code!");

  // Start kernel code

  LOG_INFO("Starting kernel...");

  ret = StartKernelCode(cmdQueue, kernel, clEvent);

  if (ret != ERROR_SUCCESS)
  {
    LOG_ERROR("Failed to start kernel code!");
    return ret;
  }

  LOG_INFO("Kernel code successfuly started!");

  return ERROR_SUCCESS;
}

int main(int argc, const char** argv)
{
  test_function("kernel.cl");
  return 0;
}