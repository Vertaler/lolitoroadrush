#pragma once
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS


#include "OpenCL\CL\cl.h"
#include <string>
#include <vector>
#include <winerror.h>

#define MAX_PLATFORM_SIZE 10
#define MAX_PLATFORM_NAME 100

cl_int GetPlatformList(cl_platform_id* platformIds, cl_uint numOfPlatforms, std::vector<std::string> &platformList);

cl_int CompileKernelCode(const char* sourcePath, cl_context context, cl_device_id device_id, cl_kernel &kernel);

cl_int StartKernelCode(cl_command_queue queue, cl_kernel kernel, cl_event &clEvent);

