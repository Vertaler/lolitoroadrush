#include <iostream>

#ifdef _DEBUG
#define LOG_INFO(x)     std::cout << "INFO:\t" << x << std::endl 
#define LOG_ERROR(x)    std::cout << "ERROR:\t" << x << std::endl
#define LOG_WARNING(x)  std::cout << "WARNING:\t" << x << std::endl
#else
#define LOG_INFO(x)   
#define LOG_ERROR(x)  
#define LOG_WARNING(x)
#endif